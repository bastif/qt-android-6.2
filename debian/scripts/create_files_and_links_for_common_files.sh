#!/bin/bash

# Create files qt-android-$VER-common.install & package.links
#
# Usage:
#   create_files_and_links_for_common_files.sh <source_dir> <destination_dir>
# Preferably one for 32bits & one for 64bits & one arm/intel
#
#   create_files_and_links_for_common_files.sh <deb_arch1.deb> <deb_arch2.deb> <deb_common.deb>
#
#   create_files_and_links_for_common_files.sh qt-android*arm64-v8a_*.deb qt-android*x86_*.deb qt-android-6.2-common*deb
#
#
#

if [ -z "$1" ] || [ -z "$2" ] || \
    ( [ -z "$3" ] && ( [[ "$1" =~ .*\.deb ]] || [[ "$2" =~ .*\.deb ]] )) || \
    ( ! [ -z "$3" ] && ! ( [[ "$1" =~ .*\.deb ]] && [[ "$2" =~ .*\.deb ]] && [[ "$3" =~ .*\.deb ]] ) ) ; then
    echo
    echo "Usage:"
    echo "  create_files_and_links_for_common_files.sh <deb_arch1.deb> <deb_arch2.deb> <deb_common.deb>"
    echo "  create_files_and_links_for_common_files.sh <source_dir> <destination_dir>"
    echo "Preferably one for 32bits & one for 64bits & one arm/intel"
    echo
    exit 1
fi

VER=6.2
VER3=6.2.3
TMP_DIR=$(mktemp -d)

if [[ "$1" =~ .*\.deb ]]; then
    EXTRACT_DIR_1=$(cut -d _ -f 1 <<< "$1")
    SUBDIR=$(LC_ALL=C dpkg -c "$1" | head -n 4 | tail -1 | cut -f 5 -d '/')
    SRC="$EXTRACT_DIR_1/usr/lib/$SUBDIR"
    dpkg-deb -x "$1" "$EXTRACT_DIR_1"
else
    SRC="$1" #"qt-android-6.2-arm64-v8a_6.2.0+ds-1_amd64/usr/lib/qt-android-6.2-arm64-v8a"
fi

if [[ "$2" =~ .*\.deb ]]; then
    EXTRACT_DIR_2=$(cut -d _ -f 1 <<< "$2")
    SUBDIR=$(LC_ALL=C dpkg -c "$2" | head -n 4 | tail -1 | cut -f 5 -d '/')
    DST="$EXTRACT_DIR_2/usr/lib/$SUBDIR"
    dpkg-deb -x "$2" "$EXTRACT_DIR_2"
else
    DST="$2" #"qt-android-6.2-x86_6.2.0+ds-1_amd64/usr/lib/qt-android-6.2-x86"
fi

if [[ "$3" =~ .*\.deb ]]; then
    dpkg-deb -x "$3" "$(cut -d _ -f 1 <<< "$1")"
    dpkg-deb -x "$3" "$(cut -d _ -f 1 <<< "$2")"
fi

# List identical files between the two directories
LC_ALL=C diff -sqr \
    "$SRC" \
    "$DST" \
    | grep "identical$" \
    | sed -e "s,$EXTRACT_DIR_1,___," -e "s,$EXTRACT_DIR_2,___," \
    | cut -d ' ' -f 2 \
    | cut -d '/' -f2- \
    | LC_ALL=C sort > "$TMP_DIR/equal"

D=$(head -n1 "$TMP_DIR/equal" | sed "s,.*usr/lib/\(.*\),\1," | cut -d '/' -f1)

COMMON_DIRS="
doc
jar
libexec
phrasebooks
src
translations
include/Qt3DAnimation
include/Qt3DExtras
include/Qt3DInput
include/Qt3DLogic
include/Qt3DQuick
include/Qt3DQuickAnimation
include/Qt3DQuickExtras
include/Qt3DQuickInput
include/Qt3DQuickRender
include/Qt3DQuickScene2D
include/Qt3DRender
include/QtBluetooth
include/QtBodymovin
include/QtCharts
include/QtChartsQml
include/QtCoap
include/QtConcurrent
include/QtCore5Compat
include/QtDataVisualization
include/QtDesigner
include/QtDesignerComponents
include/QtDeviceDiscoverySupport
include/QtFbSupport
include/QtGui
include/QtHelp
include/QtInputSupport
include/QtLabsAnimation
include/QtLabsFolderListModel
include/QtLabsQmlModels
include/QtLabsSettings
include/QtLabsSharedImage
include/QtLabsWavefrontMesh
include/QtMqtt
include/QtMultimedia
include/QtMultimediaQuick
include/QtMultimediaWidgets
include/QtNetwork
include/QtNetworkAuth
include/QtNfc
include/QtOpcUa
include/QtOpenGL
include/QtOpenGLWidgets
include/QtPacketProtocol
include/QtPositioning
include/QtPositioningQuick
include/QtPrintSupport
include/QtQmlCompiler
include/QtQmlCore
include/QtQmlDebug
include/QtQmlDom
include/QtQmlLocalStorage
include/QtQmlModels
include/QtQmlWorkerScript
include/QtQmlXmlListModel
include/QtQuick
include/QtQuick3D
include/QtQuick3DAssetImport
include/QtQuick3DAssetUtils
include/QtQuick3DHelpers
include/QtQuick3DIblBaker
include/QtQuick3DParticles
include/QtQuick3DRuntimeRender
include/QtQuick3DUtils
include/QtQuickControls2
include/QtQuickControls2Impl
include/QtQuickDialogs2
include/QtQuickDialogs2QuickImpl
include/QtQuickDialogs2Utils
include/QtQuickLayouts
include/QtQuickParticles
include/QtQuickShapes
include/QtQuickTemplates2
include/QtQuickTest
include/QtQuickTimeline
include/QtQuickWidgets
include/QtRemoteObjects
include/QtRemoteObjectsQml
include/QtRepParser
include/QtScxml
include/QtScxmlQml
include/QtSensors
include/QtSensorsQuick
include/QtSerialBus
include/QtSerialPort
include/QtShaderTools
include/QtSql
include/QtStateMachine
include/QtStateMachineQml
include/QtSvg
include/QtSvgWidgets
include/QtTest
include/QtTools
include/QtUiPlugin
include/QtUiTools
include/QtVirtualKeyboard
include/QtWebChannel
include/QtWebSockets
include/QtWebView
include/QtWebViewQuick
include/QtWidgets
include/QtXml
lib/metatypes
mkspecs/aix-g++
mkspecs/aix-g++-64
mkspecs/android-clang
mkspecs/common
mkspecs/cygwin-g++
mkspecs/darwin-g++
mkspecs/devices
mkspecs/dummy
mkspecs/features
mkspecs/freebsd-clang
mkspecs/freebsd-g++
mkspecs/haiku-g++
mkspecs/hpuxi-g++-64
mkspecs/hurd-g++
mkspecs/integrity-armv7
mkspecs/integrity-armv7-imx6
mkspecs/integrity-armv8-rcar
mkspecs/integrity-x86
mkspecs/linux-aarch64-gnu-g++
mkspecs/linux-arm-gnueabi-g++
mkspecs/linux-clang
mkspecs/linux-clang-32
mkspecs/linux-clang-libc++
mkspecs/linux-clang-libc++-32
mkspecs/linux-g++
mkspecs/linux-g++-32
mkspecs/linux-g++-64
mkspecs/linux-icc
mkspecs/linux-icc-32
mkspecs/linux-icc-64
mkspecs/linux-icc-k1om
mkspecs/linux-llvm
mkspecs/linux-lsb-g++
mkspecs/lynxos-g++
mkspecs/macx-clang
mkspecs/macx-g++
mkspecs/macx-icc
mkspecs/macx-ios-clang
mkspecs/macx-tvos-clang
mkspecs/macx-watchos-clang
mkspecs/macx-xcode
mkspecs/netbsd-g++
mkspecs/openbsd-g++
mkspecs/qnx-aarch64le-qcc
mkspecs/qnx-armle-v7-qcc
mkspecs/qnx-x86-64-qcc
mkspecs/qnx-x86-qcc
mkspecs/qtdoc_dummy_file.txt
mkspecs/solaris-cc
mkspecs/solaris-cc-64
mkspecs/solaris-cc-64-stlport
mkspecs/solaris-cc-stlport
mkspecs/solaris-g++
mkspecs/solaris-g++-64
mkspecs/unsupported
mkspecs/wasm-emscripten
mkspecs/win32-arm64-msvc
mkspecs/win32-clang-g++
mkspecs/win32-clang-msvc
mkspecs/win32-g++
mkspecs/win32-icc
mkspecs/win32-icc-k1om
mkspecs/win32-msvc
qml/QtCharts/designer
qml/QtDataVisualization/designer
qml/QtQuick3D/designer
qml/QtQuick3D/Effects/designer
qml/QtQuick3D/Helpers/meshes
qml/QtQuick3D/Particles3D/designer
"

##############
# Not used. Would require to put all the folders that have differences in the .install file
# Otherwise dh_link would report this error:
#  → link destination debian/qt-android-6.2-x86/usr/lib/qt-android-6.2-x86/qml/QtQuick/Dialogs/quickimpl/qml is a directory
##############
# qml/QtQuick/Dialogs/quickimpl/qml
# qml/QtQuick/Controls/designer
# qml/QtQuick/NativeStyle/controls
##############

# These files maybe identical between armv7 & armv8, but will differ with x86/x64
# the .*ConfigVersion.cmake files differ only between 32/64 bits (whichever arch)
NOT_ALWAYS_IDENTICAL="
include/Qt3DCore/6.2.3/Qt3DCore/private/qt3dcore-config_p.h
include/QtQml/6.2.3/QtQml/private/qtqml-config_p.h
include/QtCore/6.2.3/QtCore/private/qtcore-config_p.h
include/QtCore/6.2.3/QtCore/private/qconfig_p.h
mkspecs/modules/qt_lib_core.pri
mkspecs/modules/qt_lib_qml_private.pri
mkspecs/modules/qt_lib_3dcore_private.pri
.*ConfigVersion.cmake
"

# Replace some files with the common directory holding them
for dir in $COMMON_DIRS; do
    PATTERN=$(sed "s,/,\\\/,g" <<< "usr/lib/$D/$dir")
    sed -i "/$PATTERN/d" "$TMP_DIR/equal"
    echo "usr/lib/$D/$dir" >> "$TMP_DIR/equal"
done

# Remove somes false positives (detected as identical with input dirs, but may be different)
for match in $NOT_ALWAYS_IDENTICAL; do
    PATTERN=$(sed "s,/,\\\/,g" <<< "usr/lib/$D/$match")
    sed -i "/$PATTERN$/d" "$TMP_DIR/equal"
done

#sed "s,^usr/lib/$D,usr/lib/*," "$TMP_DIR/equal" > qt-android-$VER-common.files
echo "# File generated by debian/scripts/create_files_and_links_for_common_files.sh" > qt-android-$VER-common.install
sed "s/\(.*\)$D\(.*\)/\1*\2 \1qt-android-$VER-common\2/" "$TMP_DIR/equal" > "$TMP_DIR/tmp"
# Remove last item of path
rev "$TMP_DIR/tmp" | cut -d / -f 2- | rev >> qt-android-$VER-common.install
rm "$TMP_DIR/tmp"

for abi in armeabi-v7a arm64-v8a x86 x86_64; do
    abi_for_package_name="$(sed "s/_/-/g" <<< "$abi")"
    echo "# File generated by debian/scripts/create_files_and_links_for_common_files.sh" > qt-android-$VER-$abi_for_package_name.links
    sed "s/\(.*\)$D\(.*\)/\1qt-android-$VER-common\2 \1qt-android-$VER-$abi\2/" "$TMP_DIR/equal" >> qt-android-$VER-$abi_for_package_name.links
done

rm "$TMP_DIR/equal"
rmdir "$TMP_DIR"
