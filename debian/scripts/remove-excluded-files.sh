#!/bin/bash

# Run this script from the directory where sources are unpacked
# Parameter: path to debian/copyright file
#
#    remove-excluded-files.sh debian/copyright
#

files="$(sed -n '/^Files-Excluded:/, /^\s*$/p' $1 | \
            grep -v ^$ | \
            sed s/Files-Excluded:// | \
            grep -v "\#" | \
            sed -e "s/^\s*//g")"

for file in $files; do
    echo "Removing: $file"
    rm -fr "$file"
done
