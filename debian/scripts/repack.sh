#!/bin/bash

# Repack upstream tarball and remove files listed in "Files-Excluded:" of debian/copyright
# This will create a "repack_orig_tar" directory at the place where this script is called.
#
# Usage:
#   repack.sh <debian_dir> <upstream_tar_path>
#

if [ -z "$1" ] || [ -z "$2" ] ; then
    echo
    echo "Usage:"
    echo "  repack.sh --upstream-version <ver> <downloaded file>"
    echo
    exit 1
fi

if [ -e "repack_orig_tar" ]; then
    echo "repack_orig_tar already exists, remove it first"
    exit 1
fi

VER="$2"
SCRIPT_PATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_PATH")
DEBIAN_DIR="$(readlink -f "$SCRIPT_DIR/..")"
UPSTREAM_TAR="$(readlink -f "$3")"

PKG=$(dpkg-parsechangelog --show-field Source)
REPACK_DIR="$PKG-${VER}"
DFSG_TAR="$(dirname "$UPSTREAM_TAR")/${PKG}_${VER}.orig.tar"

# .gz or .bz2?
FILETYPE=$(file --brief --mime-type --dereference "$UPSTREAM_TAR")
case "$FILETYPE" in
    application/x-gzip|application/gzip|application/zip)
        C_ID="z"
        C_SUFFIX="gz"
        ;;
    application/x-bzip2|application/bzip2)
        C_ID="j"
        C_SUFFIX="bz2"
        ;;
    application/x-xz|application/xz)
        C_ID="J"
        C_SUFFIX="xz"
        ;;
    *)
        echo "E: Unknown filetye $FILETYPE"
        exit 1
        ;;
esac

if [ ! -e "$UPSTREAM_TAR.repacked" ] ; then
    echo "Unpacking $(basename "$UPSTREAM_TAR") to repack it without Files-Excluded & unused modules..." &&
    mkdir "repack_orig_tar" &&
    TAR_FIRST_DIR="$(tar -tf "$UPSTREAM_TAR" | head -n1 | cut -d / -f 1)" &&

    tar -xf "$UPSTREAM_TAR" \
        -C "repack_orig_tar" \
        --exclude "$TAR_FIRST_DIR/qtwebengine" \
        --exclude "$TAR_FIRST_DIR/qtwayland" &&

    # Rename main folder
    mv "repack_orig_tar/${TAR_FIRST_DIR}" "repack_orig_tar/${REPACK_DIR}" &&
    TAR_FIRST_DIR="${REPACK_DIR}" &&

    cd "repack_orig_tar/$TAR_FIRST_DIR" &&
    "$DEBIAN_DIR/scripts/remove-excluded-files.sh" "$DEBIAN_DIR/copyright" &&
    cd .. &&

    echo "Size of $UPSTREAM_TAR" &&
    ls -alh "$UPSTREAM_TAR" &&

    echo "Repacking $(basename "$UPSTREAM_TAR") without Files-Excluded & unused modules..." &&
    tar --owner=qt:1000 --group=qt:1000 -c${C_ID}f "${DFSG_TAR}.${C_SUFFIX}" "$TAR_FIRST_DIR" &&
    touch "$UPSTREAM_TAR.repacked" &&

    cd .. &&
    echo "Removing repack_orig_tar" &&
    rm -r "repack_orig_tar" &&

    echo "Size of $UPSTREAM_TAR" &&
    ls -alh "$UPSTREAM_TAR" &&
    echo "Size of ${DFSG_TAR}.${C_SUFFIX}" &&
    ls -alh "${DFSG_TAR}.${C_SUFFIX}"
else
    echo "File is already repacked (file $UPSTREAM_TAR.repacked is present)"
fi
