============================
 To compile Qt6 for Android
============================

You will need Qt sources, Android SDK and Android NDK. Get more information in the README.md file of the master branch of this repository, or go here: https://salsa.debian.org/bastif/qt-android/-/blob/master/README.md

Qt upstream sources are repacked to remove non-free files and reduce its size (Qt6.2.0 archive if reduced from 636 MiB to 279 MiB) . Removed files are listed in debian/copyright file. You can repack the upstream source by using the script in debian/scripts/repack.sh.

This has been tested with Qt 6.2.0, Android NDK r22b.

Double check the minimal Android SDK platform requirement at https://salsa.debian.org/bastif/qt-android/-/blob/master/README.md . You may not be able to use Debian's Android SDK package through build profile "pkg.qt-android-6.2.systemsdk".

You need first to create a Qt6 package that will run on the system.
This package is needed to crosscompile Qt6 for android.
It is also the package that contains androiddeployqt binary.

To build the package, use sbuild or dpkg-buildpackage with the following build profiles and build options.
Set the following environment variables:

-------------------
DEB_BUILD_PROFILES:
-------------------
Possible values are:

pkg.qt-android-6.2.host
pkg.qt-android-6.2.armeabi-v7a
pkg.qt-android-6.2.arm64-v8a
pkg.qt-android-6.2.x86
pkg.qt-android-6.2.x86_64
pkg.qt-android-6.2.systemsdk

You can only use one profile at a time, except for pkg.qt-android-6.2.systemsdk

pkg.qt-android-6.2.systemsdk can be added to the profiles for the Android ABI. It permits to add the build dependency to android-sdk-* debian packages.

------------------
DEB_BUILD_OPTIONS:
------------------
Possible values are:

android_sdk_root=/opt/android-sdk
android_ndk_root=/opt/android-sdk/ndk/22.1.7171670
android_ndk_platform=android-22
qt_conf_enable_ccache

android_sdk_root: if not set it uses /usr/lib/android-sdk
android_ndk_root: must be set to the path where the NDK version to use NDK is located.
We don't use debian's NDK because it would require network access to download it from google and on build systems there is no such access

android_ndk_platform: optional. Should be in the form of 'android-<API level>'. For example: "android_ndk_platform=android-22"
If you set the value to "default" it will use the value that qt uses by default.

qt_conf_enable_ccache: add '--ccache=yes' to the ./configure command of Qt

------------------
EXAMPLES
------------------

# to build pkg.qt-android-6.2.host profile
---------------------------------------------------

    ARCH=amd64
    DISTRIB=bullseye
    DEB_BUILD_PROFILES="pkg.qt-android-6.2.host" \
    sbuild --arch=$ARCH -d $DISTRIB

or

    DEB_BUILD_PROFILES="pkg.qt-android-6.2.host" dpkg-buildpackage -us -uc

or

    dpkg-buildpackage -us -uc -Ppkg.qt-android-6.2.host


# To build qt-android for each android ABI (each profile should be built separately)
------------------------------------------------------------------------------------
pkg.qt-android-6.2.armeabi-v7a
pkg.qt-android-6.2.arm64-v8a
pkg.qt-android-6.2.x86
pkg.qt-android-6.2.x86_64

    ARCH=amd64
    DISTRIB=bullseye
    DEB_BUILD_OPTIONS="android_sdk_root=/opt/android-sdk \
        android_ndk_root=/opt/android-sdk/ndk/22.1.7171670" \
    DEB_BUILD_PROFILES="pkg.qt-android-6.2.x86" \
    sbuild --arch=$ARCH -d $DISTRIB \
        --extra-package=PATH_TO_QT_FOR_HOST/qt-android-6.2-host_6.2.0-1_amd64.deb

or

    DEB_BUILD_OPTIONS="android_sdk_root=/opt/android-sdk \
        android_ndk_root=/opt/android-sdk/ndk/22.1.7171670" \
    DEB_BUILD_PROFILES="pkg.qt-android-6.2.x86" \
    dpkg-buildpackage -us -uc

or

    DEB_BUILD_OPTIONS="android_sdk_root=/opt/android-sdk \
        android_ndk_root=/opt/android-sdk/ndk/22.1.7171670" \
    dpkg-buildpackage -us -uc -Ppkg.qt-android-6.2.x86

or with logging

    for prof in armeabi-v7a arm64-v8a x86 x86_64; do
        touch ../$prof.start;
        DEB_BUILD_OPTIONS="android_sdk_root=/opt/android-sdk \
            android_ndk_root=/opt/android-sdk/ndk/22.1.7171670" \
        dpkg-buildpackage -us -uc -Ppkg.qt-android-6.2.$prof > ../$prof.log 2>&1 ;
        touch ../$prof.end;
    done


==================================================
 Procedure to update to a new version of upstream
==================================================

1. Download & repack latest upstream tarball with command below (it will launch scripts/repack.sh):
    `uscan --verbose --download --no-exclusion --no-symlink`
1. Update d/changelog
    `dch -v 6.2.3+ds-1`
1. Modify files d/*.install and d/*.links beacause they contain folder with version number (eg .../6.2.2/...)
    `for file in $(find . -name "*.links" -or -name "*.install"); do sed -i -e "s/6\.2\.2/6.2.3/g" $file ; done`
1. Update d/copyright with the script update-copyright
    `tar -xvf ../qt-android-6.2_6.2.3+ds.orig.tar.xz --strip-component 1`
    `./debian/scripts/update-copyright`
1. Check copyright file with debmake -k:
    `debmake -k > _license_check.txt'
1. Refresh patches & check if they are still relevant:
    `while dquilt push; do dquilt refresh; done`
    `while dquilt pop; do echo "removing" ; done`
1. Commit & upload changes to salsa & launch pipeline
1. Check build logs for output of `dh_missing`
1. Download the artifacts of the aptly jobs
1. Launch script that searches for duplicates (for common package) & update d/*.install and d/*.links accordingly
    `./debian/scripts/create_files_and_links_for_common_files.sh \
      aptly-arm8/pool/main/q/qt-android-6.2/qt-android-6.2-arm64-v8a_6.2.3+ds-1_amd64.deb \
      aptly-x86/pool/main/q/qt-android-6.2/qt-android-6.2-x86_6.2.3+ds-1_amd64.deb \
      aptly-arm7/pool/main/q/qt-android-6.2/qt-android-6.2-common_6.2.3+ds-1_amd64.deb`
    `cp *.links *.install debian/`
1. Run the lintian jobs & check everything is fine (for example the copyright file...)
1. Commit & upload changes to salsa & launch pipeline
